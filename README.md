<div  align="center">
    <img width="250" src="./src/assets/images/angularjs.png">
</div>

---

### Desafio MV 

Utilizando boas práticas de desenvolvimento no software, crie uma aplicação para ler e apresentar dados do Cadastro Nacional de Estabelecimentos de Saúde (CNES) no Brasil.
Fonte dos dados: http://dados.gov.br/dataset/cnes_ativo

###  mv-cnes-frontend

Plataforma web (SPA) desenvolvida com Angular, TypeScript e Atomic Design.

O Atomic design é uma metodologia desenvolvida para a criação de design systems. Ela é composta por cinco estágios, trabalhando juntos para criar interfaces de maneira deliberada ehierárquica. 

Os cinco estágios do Atomic Design são:

    - Atoms
    - Molecules
    - Orgnisms
    - Templates
    - Page

---

#### Pré-requisitos

Para inicializar o mv-cnes-frontend é necessário ter o `Node.js` instalado (versão 10.x ou acima) e o seu gerenciador de pacotes favorito na versão mais atual. Caso você ainda não tenha instalado o pacote `@angular/cli`, instale-o via `npm` ou `yarn`.

Instalando com npm:
```
npm i -g @angular/cli@^10.0.1
```

Caso prefira instalar com o yarn:
```
yarn global add @angular/cli@^10.0.1
```

#### Passo 1 - Instalando as dependências

Acesse a pasta raiz do projeto e execute o comando abaixo:

Instalando com npm:
```
npm install
```

Caso prefira instalar com o yarn:
```
yarn install
```

#### Passo 2 - Start o projeto

Agora basta executar o comando para subir a aplicação e ver o projeto rodando no *browser* ;).

```
ng serve
```

Abra o *browser* e acesse a url http://localhost:4200. Pronto!

----

### Hospedagem - Heroku

Heroku é uma plataforma na nuvem que faz deploy de várias aplicações Back-end e Front-end seja para hospedagem, testes em produção ou escalar as suas aplicações. Também tem integração com o GitHub, deixando o uso mais fácil e com containers denominados Dyno.

Para aplicação `mv-cnes-frontend` foi utilizado um `Free Dynos` ideal para experimentar aplicativos na nuvem em uma sandbox limitada que dorme após 30 min de inatividade.


Domínio

> https://mv-cnes-frontend.herokuapp.com/


### License
[MIT](https://choosealicense.com/licenses/mit/)



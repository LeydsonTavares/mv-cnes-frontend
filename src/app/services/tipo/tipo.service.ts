import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TipoService {

    constructor(private https : HttpClient) {}

  /**
   * Recuperar todos estabelecimentos por tipo
   * @param tipo - representa o tipo da Gestão selecionado.
   */
  listarCNESPorTipo(tipo : string){
    return this.https.get(`${environment.apiUrl}/gestao/${tipo}`);
  }  

  /**
   * Recuperar tipos da Gestões
   */
  listarTiposGestao(){
    return [
      {"nome": "Municipal", "value": "M"},
      {"nome": "Estadual", "value": "E"} ,  
      {"nome": "Dupla", "value": "D"}      
    ]
  }

}
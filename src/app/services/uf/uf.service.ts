import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UfService {

    constructor(private https : HttpClient) {}

  /**
   * Recuperar todos registros por estado
   * @param sigla - representa a sigla do estado selecionado.
   */
  listarCNESPorUf(sigla : string){
    return this.https.get(environment.apiUrl + sigla);
  }  

  /**
   * Recuperar todos estados brasileiros
   */
  listarEstados(){
    return [
      { "src":"assets/images/bandeira-acre.png","nome": "Acre", "sigla": "AC"},
      { "src":"assets/images/bandeira-alagoas.png","nome": "Alagoas", "sigla": "AL"},
      { "src":"assets/images/bandeira-amapa.png","nome": "Amapá", "sigla": "AP"},
      { "src":"assets/images/bandeira-amazonas.png","nome": "Amazonas", "sigla": "AM"},
      { "src":"assets/images/bandeira-bahia.png","nome": "Bahia", "sigla": "BA"},
      { "src":"assets/images/bandeira-ceara.png","nome": "Ceará", "sigla": "CE"},
      { "src":"assets/images/bandeira-distrito-federal.png","nome": "Distrito Federal", "sigla": "DF"},
      { "src":"assets/images/bandeira-espirito-santo.png","nome": "Espírito Santo", "sigla": "ES"},
      { "src":"assets/images/bandeira-goias.png","nome": "Goiás", "sigla": "GO"},
      { "src":"assets/images/bandeira-maranhao.png","nome": "Maranhão", "sigla": "MA"},
      { "src":"assets/images/bandeira-mato-grosso.png","nome": "Mato Grosso", "sigla": "MT"},
      { "src":"assets/images/bandeira-mato-grosso-do-sul.png","nome": "Mato Grosso do Sul", "sigla": "MS"},
      { "src":"assets/images/bandeira-minas-gerais.png","nome": "Minas Gerais", "sigla": "MG"},
      { "src":"assets/images/bandeira-para.png","nome": "Pará", "sigla": "PA"},
      { "src":"assets/images/bandeira-paraiba.png","nome": "Paraíba", "sigla": "PB"},
      { "src":"assets/images/bandeira-parana.png","nome": "Paraná", "sigla": "PR"},
      { "src":"assets/images/bandeira-pernambuco.png","nome": "Pernambuco", "sigla": "PE"},
      { "src":"assets/images/bandeira-piaui.png","nome": "Piauí", "sigla": "PI"},
      { "src":"assets/images/bandeira-rio-de-janeiro.png","nome": "Rio de Janeiro", "sigla": "RJ"},
      { "src":"assets/images/bandeira-rio-grande-do-norte.png","nome": "Rio Grande do Norte", "sigla": "RN"},
      { "src":"assets/images/bandeira-rio-grande-do-sul.png","nome": "Rio Grande do Sul", "sigla": "RS"},
      { "src":"assets/images/bandeira-rondonia.png","nome": "Rondônia", "sigla": "RO"},
      { "src":"assets/images/bandeira-roraima.png","nome": "Roraima", "sigla": "RR"},
      { "src":"assets/images/bandeira-santa-catarina.png","nome": "Santa Catarina", "sigla": "SC"},
      { "src":"assets/images/bandeira-sao-paulo.png","nome": "São Paulo", "sigla": "SP"},
      { "src":"assets/images/bandeira-sergipe.png","nome": "Sergipe", "sigla": "SE"},
      { "src":"assets/images/bandeira-tocantins.png","nome": "Tocantins", "sigla": "TO"}
    ]
  }
}
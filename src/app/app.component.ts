import { Component } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { environment } from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  constructor(private https : HttpClient) {

    if(environment.production){
      const headers = new HttpHeaders({'Access-Control-Allow-Origin':'*'});
      // Request acionada a cada 10 min para evitar que o serve do Heroku fique inativo
      setInterval(() => {
        this.https.get("https://mv-cnes-frontend.herokuapp.com", {headers: headers}).subscribe();
      }, 600000);
    }
  }

}

import {Component, Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


export interface DialogData {
  siglaEstado: string;
  nomeLogradouro: string;
  tipoGestao: string;
  numeroEndereco: string;
  nomeBairro: string;
  codigoCEP: string;
  municipio: string;
  numeroTelefone: string;
}

@Component({
  templateUrl: 'dialog.component.html',
})
export class DialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}



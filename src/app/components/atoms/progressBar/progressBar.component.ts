import {Component} from '@angular/core';

/**
 * @title Indeterminate progress-bar
 */
@Component({
  selector: 'app-progress-bar',
  templateUrl: 'progressBar.component.html',
})
export class ProgressBarComponent {}
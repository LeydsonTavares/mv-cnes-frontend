import {Component} from '@angular/core';

/**
 * @title Indeterminate progress-bar
 */
@Component({
  selector: 'app-loader',
  templateUrl: 'loader.component.html',
})
export class LoaderComponent {

}
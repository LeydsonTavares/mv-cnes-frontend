import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import { NgxPaginationModule } from 'ngx-pagination';
import { ListaEstabelecimentoComponent } from './lista-estabelecimento.component';
import { LoaderComponent } from '../../atoms/loader/loader.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Lista de estabelecimentos de saúde contidos no CNES',
            urls: [
                { title: 'Estabelecimentos', url: '/estabelecimentos' },
                { title: 'Estabelecimentos' }
            ]
        },
        component: ListaEstabelecimentoComponent
    }
];

@NgModule({
    imports: [FormsModule, CommonModule, RouterModule.forChild(routes),NgxPaginationModule, MatDialogModule],
    declarations: [ListaEstabelecimentoComponent,LoaderComponent]
})
export class ListaEstabelecimentoModule { }

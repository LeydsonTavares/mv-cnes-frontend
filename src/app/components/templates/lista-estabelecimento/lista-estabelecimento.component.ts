import { Component, OnInit } from '@angular/core';
import {ListaEstabelecimentoService} from '../../../services/estabelecimento/lista-estabelecimento.service'
import {DialogComponent } from '../../molecules/dialog/dialog.component';
import {MatDialog } from '@angular/material/dialog';

@Component({
	templateUrl: 'lista-estabelecimento.component.html'
})
export class ListaEstabelecimentoComponent implements OnInit {

	isLoading = true;
	error = false
	estabelecimentos  : any = [];
	pag : Number = 1 ;
	contador : Number = 100;

	constructor(private listaEstabelecimentoService : ListaEstabelecimentoService, public dialog: MatDialog) { }


	ngOnInit(): void {
		this.listaEstabelecimentoService.listarEstabelecimento().subscribe((res) => {
			this.estabelecimentos = res;
			this.isLoading = false;
		  },
		  (err) => {
			this.isLoading =false;
			this.error = true;
		  });
	}

	openDialog(item: any): void {
		  this.dialog.open(DialogComponent, {
			width: '100%',
			data: item
		  });
	  
	}
	
}

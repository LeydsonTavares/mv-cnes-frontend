import { Component, AfterViewInit, OnInit } from '@angular/core';
import {ListaEstabelecimentoService} from '../../../services/estabelecimento/lista-estabelecimento.service'
import { map, tap, filter, scan, retry, catchError } from 'rxjs/operators';

@Component({
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit  {

    isLoading = true;
    error = false
    estabelecimentos  : any = [];
    municipais : any = [];
    estaduais : any = [];
    dupla : any = [];
    porcentagemDupla = "";
    porcentagemMunicipal = "";
    porcentagemEstado = "";
    doughnutChartLabels: any = [];
    doughnutChartOptions = {};
    doughnutChartData: any = [];
    doughnutChartType = "doughnut"
    doughnutChartLegend = false;


    constructor(private listaEstabelecimentoService : ListaEstabelecimentoService) { }

    ngOnInit(): void {
        this.listaEstabelecimentoService.listarEstabelecimento().subscribe((response: any = []) => {
			this.isLoading = false;
            this.estaduais = response.filter((item: any) => item.tipoGestao === "E");
            this.dupla = response.filter((item: any) => item.tipoGestao === "D");
            this.municipais =  response.filter((item: any) => item.tipoGestao === "M");
            this.doughnutChartData = [this.municipais.length, this.estaduais.length, this.dupla.length];
            this.porcentagemDupla = this.percentage(this.dupla.length,response.length);
            this.porcentagemMunicipal = this.percentage(this.municipais.length,response.length);
            this.porcentagemEstado = this.percentage(this.estaduais.length,response.length);
            this.doughnutChartLabels = ['Municipal', 'Estadual', 'Dupla'];
            this.doughnutChartOptions = {
                  borderWidth: 1,
                  maintainAspectRatio: false
              };
            this.doughnutChartData = [this.municipais.length, this.estaduais.length, this.dupla.length]; 
		  },
		  (err) => {
			this.isLoading =false;
			this.error = true;
          });

    }
    
    percentage(partialValue: any, total: any) {
        return ((100 * partialValue) / total).toFixed(2);
    } 


}

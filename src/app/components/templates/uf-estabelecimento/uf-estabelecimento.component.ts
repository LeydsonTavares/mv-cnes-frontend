import { Component, OnInit} from '@angular/core';
import {UfService} from '../../../services/uf/uf.service'
import {DialogComponent } from '../../molecules/dialog/dialog.component';
import {MatDialog } from '@angular/material/dialog';


@Component({
  templateUrl: './uf-estabelecimento.component.html'
})
export class UfEstabelecimentoComponent implements OnInit  {

  isLoading = false;
  error = false;
  currentUF = '';
  estabelecimentosUf  : any = [];
  uf = {};
	pag : Number = 1 ;
  contador : Number = 50;

	constructor(private ufService : UfService, public dialog: MatDialog) { 
  }

  ngOnInit(): void {
    this.uf = this.ufService.listarEstados();

  }
    
  openDialog(item: any): void {
      this.dialog.open(DialogComponent, {
        width: '100%',
        data: item
      });
  
  }

  buscarCNESPorEstado(event: any ){
    this.isLoading =true; 
    this.error = false;
    this.estabelecimentosUf = [];
    this.ufService.listarCNESPorUf(event.sigla).subscribe((res) => {
      this.estabelecimentosUf = res;
      this.currentUF = event.nome;
      this.isLoading = false;
    },
    (err) => {
      this.currentUF = "";
      this.isLoading =false;
      this.error = true;
    });
  }

  // public uf : any [] = this.ufService.listarEstados(); 


}
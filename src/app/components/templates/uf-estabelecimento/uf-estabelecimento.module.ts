import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { UfEstabelecimentoComponent } from './uf-estabelecimento.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import { ProgressBarComponent } from '../../atoms/progressBar/progressBar.component';


const routes: Routes = [
    {
        path: '',
        data: {
            title: 'CNES por Unidade da Federação',
            urls: [
                { title: 'Unidade da Federação', url: '/uf' },
                { title: 'Unidade da Federação' }
            ]
        },
        component: UfEstabelecimentoComponent
    }
];

@NgModule({
    imports: [FormsModule, CommonModule, RouterModule.forChild(routes),NgxPaginationModule,MatProgressBarModule,MatDialogModule],
    declarations: [UfEstabelecimentoComponent, ProgressBarComponent]
})
export class UfEstabelecimentoModule { }

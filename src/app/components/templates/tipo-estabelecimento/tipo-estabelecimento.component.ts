import { Component , OnInit} from '@angular/core';
import {TipoService} from '../../../services/tipo/tipo.service'
import {DialogComponent } from '../../molecules/dialog/dialog.component';
import {MatDialog } from '@angular/material/dialog';

@Component({
	templateUrl: './tipo-estabelecimento.component.html'
})
export class TipoEstabelecimentoComponent implements OnInit {
		isLoading = false;
		itemSelected = {};
		error = false;
		tipoAcionado = {};
		tipos : any = [];
		estabelecimentosTipo  : any = [];
		pag : Number = 1 ;
		contador : Number = 50;
	  
		constructor(private tipoService : TipoService, public dialog: MatDialog) { }
		
		ngOnInit(): void {
			this.tipos = this.tipoService.listarTiposGestao();

		}
		  
		openDialog(item: any): void {
			this.dialog.open(DialogComponent, {
			  width: '100%',
			  data: item
			});

		}
	  
		buscarCNESPorTipo(event: any ){
		  this.tipoAcionado = event;
		  this.isLoading = true; 
		  this.error = false;
		  this.estabelecimentosTipo = [];
		  this.tipoService.listarCNESPorTipo(event.value).subscribe((res) => {
			this.estabelecimentosTipo = res;
			this.isLoading = false;
		  },
		  (err) => {
			this.tipoAcionado = {};
			this.isLoading =false;
			this.error = true;
		  });
		}
	}
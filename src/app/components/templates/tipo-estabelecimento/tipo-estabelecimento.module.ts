import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { TipoEstabelecimentoComponent } from './tipo-estabelecimento.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';



const routes: Routes = [
    {
        path: '',
        data: {
            title: 'CNES por Tipo de Gestão',
            urls: [
                { title: 'Tipo de Gestão', url: '/tipo-gestao' },
                { title: 'Tipo de Gestão' }
            ]
        },
        component: TipoEstabelecimentoComponent
    }
];

@NgModule({
    imports: [FormsModule, CommonModule, RouterModule.forChild(routes),NgxPaginationModule,MatProgressBarModule,MatDialogModule],
    declarations: [TipoEstabelecimentoComponent],

    
})
export class TipoEstabelecimentoModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FullComponent } from './containers/pages/full/full.component';

export const Approutes: Routes = [
    {
        path: '',
        component: FullComponent,
        children: [
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
            {
                path: 'dashboard',
                loadChildren: () => import('./components/templates/dashboard/dashboard.module').then(m => m.DashboardModule)
            },
            {
                path: 'estabelecimentos',
                loadChildren: () => import('./components/templates/lista-estabelecimento/lista-estabelecimento.module').then(m => m.ListaEstabelecimentoModule)
            },
            {
                path: 'tipo-gestao',
                loadChildren: () => import('./components/templates/tipo-estabelecimento/tipo-estabelecimento.module').then(m => m.TipoEstabelecimentoModule)
            }
            ,
            {
                path: 'uf',
                loadChildren: () => import('./components/templates/uf-estabelecimento/uf-estabelecimento.module').then(m => m.UfEstabelecimentoModule)
            }
        ]
    },
    {
        path: '**',
        redirectTo: '/dashboard'
    }
];
